<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="Kurjeris/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="Kurjeris/css/styles.css">
<link rel="stylesheet" type="text/css" href="Klientas/css/styles.css">
<link rel="stylesheet" type="text/css" href="Darbuotojas/css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
</style>
</head>
<body background="Klientas/css/bg1.jpg">
<div id="alls">
<div id="alls2">
	<div id="customheader">
		<div class="container">
			<h2>Speed food</h2>
			<p>Maisto užsakymo į namus sistema</p>
		</div>
	</div>
		<div class="container">
				<h3>Sveiki atvyke!</h3>

			<div class="form">
			<h4>Prisijunkite kaip:</h4>
			<a href="Kurjeris/" class="btn btn-primary btn-block" role="button">Kurjeris</a><br>
			<a href="Klientas/" class="btn btn-primary btn-block" role="button">Klientas</a><br>
			<a href="Buhalteris/" class="btn btn-primary btn-block" role="button">Buhalteris</a><br>
			<a href="Admin/" class="btn btn-primary btn-block" role="button">Administratorius</a>
			</div>
		</div>
	</div>
</div>
<footer id="footer">
	
</footer>

</body>
</html>
