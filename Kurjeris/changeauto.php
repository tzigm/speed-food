<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Speed food</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
#styled-select {
      width: 260px;
      height: 34px;
      overflow: hidden;
      background-color: #FFF;
      border: 1px solid #ccc;

   }
   #styled-select select {
      background: transparent;
      width: 260px;
      padding: 5px;
      font-size: 15px;
      line-height: 1;
      border: 0;
      border-radius: 0;
      height: 34px;
      -webkit-appearance: none;
      font-family:helvetica-roman;
      color:#111;
   }

.table td {
   text-align: center;
}

#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
</style>
</head>
<body background="css/bg1.jpg">
<div id="alls">
<div id="alls2">

  <div id="customheader">
        <div class="container">
          <h2>Speed food</h2>
          <p>Maisto užsakymo į namus sistema</p>
        </div>
  </div>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Speed Food</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="index.php">Gauti užsakymai</a></li>
        <li><a href="acceptedorders.php">Priimti užsakymai</a></li>
        <li class="active"><a href="auto.php">Automobilis</a></li>
        <li><a href="worksheet.php">Kurjerio duomenys</a></li>
        <li><a href="top.php">Top adresai</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
      </ul>
    </div>
  </nav>

<div class="container">

<h3>Pasirinkite automobilį:</h3>
<?php
include("orderControl.php");
$controller=new OrderController;
$controller->selectAuto();
?>
</div>
</div>
</div>
<footer id="footer">
  </footer>
</body>
</html>
