<?php
// useredit.php
// vartotojas gali pasikeisti slaptažodį ar email
// formos reikšmes tikrins procuseredit.php. Esant klaidų pakartotinai rodant formą rodomos ir klaidos

session_start();
// cia sesijos kontrole
if (!isset($_SESSION['prev']) || (($_SESSION['prev'] != "index") && ($_SESSION['prev'] != "procuseredit")))
{ header("Location: logout.php");exit;}

if ($_SESSION['prev'] == "index")
	{$_SESSION['mail_login'] = $_SESSION['umail'];
	$_SESSION['passn_error'] = "";      // papildomi kintamieji naujam password įsiminti
	$_SESSION['passn_login'] = ""; }  //visos kitos turetų būti tuščios
    $_SESSION['prev'] = "useredit";
?>

<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8">
		<title>Registracija</title>
		<link href="include/styles.css" rel="stylesheet" type="text/css" >
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	</head>
	<body>
		<div id="customheader">
			<div class="container">
				<h2>Renginių kalendorius</h2>
				<p>Parengė: Paulius Melaika IFF-5/2</p>
			</div>
		</div>
		<div class="container">
		<nav aria-label="breadcrumb" role="navigation">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Pradžia</a></li>
				<li class="breadcrumb-item active" aria-current="page">Redeguoti paskyrą</li>
			</ol>
		</nav>
			<?php
				if($_SESSION['message'] != "") {
					echo "<div class=\"alert alert-danger\" role=\"alert\">";
					echo $_SESSION['message'];
					echo "</div>";
				}
			?>
			<div class="card">
				<div class="card-header">
					Vartotojo "<?php echo $_SESSION['user'];  ?>" paskyros redagavimas
				</div>
				<div class="card-body">
				<form action="procuseredit.php" method="POST">
				<div class="form-group">
					<label for="login">Dabartinis slaptažodis</label>
					<input class ="form-control" name="passn" type="password" value="<?php echo $_SESSION['passn_login'];  ?>"/><br>
					<small><?php echo $_SESSION['passn_error']; ?></small>
				</div>
				<div class="from-group">
					<label for="login">Naujas slaptažodis</label>
					<input class ="form-control" name="passn" type="password" value="<?php echo $_SESSION['passn_login']; ?>"/><br>
					<small><?php echo $_SESSION['passn_error']; ?></small>
				</div>
				<div class="from-group">
					<label for="login">El. paštas</label>
					<input class ="form-control" name="email" type="email" value="<?php echo $_SESSION['mail_login']; ?>"/><br>
					<small><?php echo $_SESSION['mail_error']; ?></small>
				</div>
				<div class="form-group">
					<button type="submit" name="login" value="Atnaujinti" class="btn btn-primary">Atnaujinti</button>
				</div>
					<?php
					//	if ($uregister != "admin") { echo "<a href=\"register.php\">Registracija</a>";}
					?>
				</form>
			</div>
		</div>

		<!-- Bootstarp JS pridejimas -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	</body>
</html>
