<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Registration</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php
	require('db.php');
    // If form submitted, insert values into the database.
    if (isset($_REQUEST['username'])){
		$username = stripslashes($_REQUEST['username']); // removes backslashes
		$username = mysqli_real_escape_string($con,$username); //escapes special characters in a string
		$email = stripslashes($_REQUEST['email']);
		$email = mysqli_real_escape_string($con,$email);
		$password = stripslashes($_REQUEST['password']);
		$password = mysqli_real_escape_string($con,$password);

		$trn_date = date("Y-m-d H:i:s");

				$query = "SELECT username from kurjeriai where `kurjeriai`.username='$username'";
				$result = mysqli_query($con,$query);
				if(mysqli_num_rows($result) > 0){
					echo "
				 <div id=\"customheader\">
							 <div class=\"container\">
								 <h2>Speed food</h2>
								 <p>Maisto užsakymo į namus sistema</p>
							 </div>
				 </div>
				 <div class='form'><h3>Vartotojas tokiu vardu jau egzistuoja.</h3><br/><a href=\"javascript:history.go(-1)\">Grįžti atgal</a></div>";
				 exit;
				}

				$query = "SELECT email from kurjeriai where `kurjeriai`.email='$email'";
				$result = mysqli_query($con,$query);
				if(mysqli_num_rows($result) >0){
					echo "
				 <div id=\"customheader\">
							 <div class=\"container\">
								 <h2>Speed food</h2>
								 <p>Maisto užsakymo į namus sistema</p>
							 </div>
				 </div>
				 <div class='form'><h3>Toks e-pašto adresas jau egzistuoja.</h3><br/><a href=\"javascript:history.go(-1)\">Grįžti atgal</a></div>";
				 exit;
			 }

        $query = "INSERT into `kurjeriai` (username, password, email, trn_date,fk_automobilio_kodas) VALUES ('$username', '".md5($password)."', '$email', '$trn_date',1)";
        $result = mysqli_query($con,$query);
        if($result){
            echo "
						<div id=\"customheader\">
						      <div class=\"container\">
						        <h2>Speed food</h2>
						        <p>Maisto užsakymo į namus sistema</p>
						      </div>
						</div>
						<div class='form'><h3>Jūsų registracija sėkminga.</h3><br/>Paspauskite čia, kad  <a href='login.php'>Prisijungti</a></div>";
        }
    }else{
?>
<div id="customheader">
      <div class="container">
        <h2>Speed food</h2>
        <p>Maisto užsakymo į namus sistema</p>
      </div>
</div>
<div class="container">
<div class="form">
<h1>Registracija</h1>
<form name="registration" action="" method="post">
<input type="text" name="username" placeholder="Username" required />
<input type="email" name="email" placeholder="Email" required />
<input type="password" name="password" placeholder="Password" required />
<input type="submit" name="submit" value="Registruotis" />
</form>
</br>
<a href="login.php">Grįžti į prisijungimo puslapį</a>
</div>
</div>
<?php } ?>
</body>
</html>
