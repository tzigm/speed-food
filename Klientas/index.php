<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Speed food</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.table td {
   text-align: center;
}
#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
</style>
</head>
<body background="css/bg1.jpg">
<div id="alls">
<div id="alls2">
  <div id="customheader">
        <div class="container">
          <h2>Speed food</h2>
          <p>Maisto užsakymo į namus sistema</p>
        </div>
  </div>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Speed Food</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Kurjerių vertinimas</a></li>
        <li><a href="order.php">Užsakymas</a></li>
        <li><a href="productevaluation.php">Prekių vertinimas</a></li>
		<li><a href="../Apmokejimai/payment-list.php">Apmokėjimų sąrašas</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
      </ul>
    </div>
  </nav>

  <div class="container">
  <?php
  $query = "SELECT `ivertinimai`.ivertis,`ivertinimai`.data, `kurjeriai`.vardas,`kurjeriai`.pavarde FROM `ivertinimai`,`kurjeriai`
  WHERE `ivertinimai`.fk_kurjerio_id=`kurjeriai`.id";
  $result = mysqli_query($con,$query) or die(mysql_error());
   if (!$result || (mysqli_num_rows($result) < 1))
       {echo "Nėra įvertinimų";}
       else{
              echo "<table class=\"table table-bordered\">";
             echo "<tr bgcolor=\"#CCC\">
              <td class=\"col-md-3\">Įvertinimas</td>
              <td class=\"col-md-3\">Data</td>
              <td class=\"col-md-3\">Kurjeris</td>
              </tr>";
         while($row = mysqli_fetch_assoc($result))
   {
     unset($ev,$date,$kurjeris);
              $ev = $row['ivertis'];
              $date = $row['data'];
              $kurjeris = $row['vardas']." ".$row['pavarde'];
          echo "<tr>
          <td>".$ev."</td>
          <td>".$date."</td>
          <td>".$kurjeris."</td>
          </tr>";
        }
		echo"</table>";
  }
   ?>
</div>
</div>
  </div>
  <footer id="footer"></footer>
</body>
</html>
