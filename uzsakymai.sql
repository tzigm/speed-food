-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017 m. Grd 03 d. 20:51
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testine`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `uzsakymai`
--

CREATE TABLE `uzsakymai` (
  `id` int(11) NOT NULL,
  `priimtas_data` date DEFAULT NULL,
  `priimtas_laikas` time DEFAULT NULL,
  `pristatytas_data` date DEFAULT NULL,
  `pristatytas_laikas` time DEFAULT NULL,
  `adresas` varchar(50) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `suma` decimal(10,2) NOT NULL,
  `busena` tinyint(1) NOT NULL,
  `fk_kurjerio_id` int(11) DEFAULT NULL,
  `fk_apmokejimo_busenos_id` int(11) DEFAULT NULL,
  `fk_kliento_vardas` varchar(30) NOT NULL,
  `budas` int(2) NOT NULL,
  `valiuta` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sukurta duomenų kopija lentelei `uzsakymai`
--

INSERT INTO `uzsakymai` (`id`, `priimtas_data`, `priimtas_laikas`, `pristatytas_data`, `pristatytas_laikas`, `adresas`, `suma`, `busena`, `fk_kurjerio_id`, `fk_apmokejimo_busenos_id`, `fk_kliento_vardas`, `budas`, `valiuta`) VALUES
(13, '2017-12-03', '19:32:33', '2017-12-03', '20:58:08', 'Studentu 69', '10.00', 2, 27, 0, 'Arnas', 0, 'Eurai'),
(14, '2017-11-20', '10:31:00', '2017-12-03', '18:52:53', 'Studentu 69', '10.00', 2, 27, 0, 'Arnas', 0, 'Eurai'),
(15, '2017-11-03', '07:33:00', '2017-11-28', '09:41:35', 'Daugirdo 38 Kaunas', '12.00', 2, 27, 0, 'Arnas', 1, 'Doleriai'),
(18, '2017-12-03', '18:38:36', '2017-12-03', '19:17:20', 'Daugirdo 38 Kaunas', '12.00', 2, 27, 1, 'Arnas', 1, 'Doleriai'),
(19, '2017-12-03', '19:09:09', '2017-12-03', '20:57:47', 'Daugirdo 38 Kaunas', '12.00', 2, 27, 1, 'Arnas', 1, 'Doleriai'),
(20, '2017-12-03', '18:21:04', '2017-12-03', '19:10:06', 'Daugirdo 38 Kaunas', '13.33', 2, 27, 1, 'Arnas', 1, 'Doleriai'),
(21, '2017-12-03', '18:40:42', '2017-12-03', '19:32:45', 'Daugirdo 38 Kaunas', '12.00', 2, 27, 1, 'Arnas', 1, 'Doleriai'),
(26, '2017-12-03', '21:50:29', '2017-12-03', '21:50:38', 'Adreso g 22', '10.00', 2, 27, 0, 'admin2', 0, 'Eurai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `uzsakymai`
--
ALTER TABLE `uzsakymai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kurjerio_id` (`fk_kurjerio_id`),
  ADD KEY `fk_kliento_vardas` (`fk_kliento_vardas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `uzsakymai`
--
ALTER TABLE `uzsakymai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `uzsakymai`
--
ALTER TABLE `uzsakymai`
  ADD CONSTRAINT `fk_kurjerioIdsa` FOREIGN KEY (`fk_kurjerio_id`) REFERENCES `kurjeriai` (`id`),
  ADD CONSTRAINT `uzsakymai_ibfk_1` FOREIGN KEY (`fk_kliento_vardas`) REFERENCES `users` (`username`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
