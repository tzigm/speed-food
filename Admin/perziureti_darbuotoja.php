<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Darbuotojo info</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../Kurjeris/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../Kurjeris/css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<stylesheet>

</stylesheet

</head>
<body>

<div id="customheader">
      <div class="container">
        <h2>Speed food</h2>
        <p>Maisto užsakymo į namus sistema</p>
      </div>
</div>
<div class="container">
      <?php
        $con = mysqli_connect("localhost","root","","testine");
        // Check connection
        if (mysqli_connect_errno())
        {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = "SELECT * FROM kurjeris WHERE kurjeris.kodas = ".$_GET['id'];
        $result = mysqli_query($con, $sql);
        $row = mysqli_fetch_assoc($result);
        if(empty($row)){
            # if user already exists
            header("Refresh: 1; http://localhost/Admin/darbtvark.php");
            echo "<script>alert('Toks darbuotojas neegzistuoja. Uzkraunamas ');</script>";
            exit;
        }

        echo "<h3>Rodoma darbuotojo \"".$row['pavarde']." ".$row['vardas']."\" informacija.</h3>";
        echo "<div class=\"form\" style=\"width: 600px\">";
        echo "<h4>Darbuotojo vardas pavarde: ".$row['vardas']." ".$row['pavarde']."<br>";
        echo "Darbuotojo ID: ".$row['kodas']."<br>";
        echo "Telefonas: ".$row['telefonas']."<br>";
        echo "Adresas: ".$row['adresas']."<br>";
        echo "</h4></div>";
        echo "<a class=\"marginright\" href=\"naikinti_darbuotoja.php?id=".$row['kodas']."\">Naikinti darbuotoja</a>";
        echo "<a class=\"marginright\" href=\"atnaujinti_informacija.php?id=".$row['kodas']."\">Atnaujinti darbuotoja</a><br>";
        ?>
</div>
</body>
</html>