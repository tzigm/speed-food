<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Speed food</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.table td {
   text-align: center;
}
</style>
</head>
<body>
  <div id="customheader">
        <div class="container">
          <h2>Speed food</h2>
          <p>Maisto užsakymo į namus sistema</p>
        </div>
  </div>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Speed Food</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="index.php">Visu darbuotoju sarasas</a></li>
        <li><a href="productevaluation.php">Marsrutu sudarymas</a></li>
        <li><a href="naujo_darbuotojo_anketa.php">Prideti nauja darbuotoja</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
      </ul>
    </div>
  </nav>

  <div class="container">
  <div class="form">
    
  </div>
        <?php include("darbuotoju_sarasas.php"); $visi_darb = new DarbuotojuSarasas(); $visi_darb->perziureti_darbuotoja($_GET['id']); ?>
  </div>
</body>
</html>
