<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Speed food</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.table td {
   text-align: center;
}
</style>
</head>
<body>
  <div id="customheader">
        <div class="container">
          <h2>Speed food</h2>
          <p>Maisto užsakymo į namus sistema</p>
        </div>
  </div>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Speed Food</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="index.php">Visu darbuotoju sarasas</a></li>
        <li><a href="productevaluation.php">Marsrutu sudarymas</a></li>
        <li class="active"><a href="naujo_darbuotojo_anketa.php">Prideti nauja darbuotoja</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
      </ul>
    </div>
  </nav>
	<div class="container">
        <h3>Naujo darbuotojo anketa</h3>
        <hr>
        <!-- ." VALUES (NULL, '".$_POST['vardas']."', 
        '".$_POST['pavarde']."', '".$_POST['lytis']."', '".$_POST['pareigos']."', 
        '".$_POST['dirba_nuo']."','".$_POST['alga']."', '".$_POST['gimimo_metai']."', 
        '".$_POST['asmens_kodas']."', '".$_POST['adresas']."', ".$kurjerioID.")"; -->
        <form action="sukurti_darbuotoja.php" method="post" required>
            Vardas:<br><input class="form-control" type="text" name="vardas" required><br>
            Pavarde:<br><input class="form-control" type="text" name="pavarde" required><br>
            Lytis:<br>
            <select name="lytis">
                <option value="V">Vyras</option>
                <option value="M">Moteris</option>
            </select>
            <br>
            <br>
            Pareigos:<br><input class="form-control" type="text" name="pareigos" required><br>
            Telefono numeris:<br><input class="form-control" type="number" name="telefonas" min="0" required><br>
            Dirba nuo:<br><input class="form-control" type="date" name="dirba_nuo" value="2017-01-01" required><br>
            Adresas:<br><input class="form-control" type="text" name="adresas" required><br>
            Alga:<br><input class="form-control" type="number" name="alga" required><br>
            Gimimo metai:<br><input class="form-control" type="date" name="gimimo_metai" value="2000-01-01" required><br>
            Asmens kodas:<br><input class="form-control" type="number" name="asmens_kodas" required><br>
            <input type="submit" value="Sukurti" required>
        </form>

    <!--
			<h3>Sveiki atvyke!</h3>

      <div class="form">
      <h4>Pasirinkite veiksma:</h4>
      <a href="Kurjeris/" class="btn btn-primary btn-block" role="button">Darbuo</a><br>
      <a href="Klientas/" class="btn btn-primary btn-block" role="button">Klientas</a><br>
      <a href="#" class="btn btn-primary btn-block" role="button">Darbuotojas</a><br>
      <a href="Admin/" class="btn btn-primary btn-block" role="button">Administratorius</a>
      </div>
    -->


	</div>

</body>
</html>