<?php
    class DarbuotojuSarasas{

        private $con;

        function __construct(){
            $this->connect_to_db();
        }

        private function connect_to_db(){
            $this->con = mysqli_connect("localhost","root","","food");
            // Check connection
            if (mysqli_connect_errno())
            {
              echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }
        }

        function gauti_visus_darbuotojus(){
            $sql = "SELECT id, vardas, pavarde, pareigos FROM darbuotoju_sarasas ORDER BY id";
            $result = mysqli_query($this->con, $sql);
            
            while($row = mysqli_fetch_assoc($result)){
                echo "<tr>";
                echo "<td>".$row['id']."</td>";
                echo "<td>".$row['pavarde']." ".$row['vardas']."</td>";
                echo "<td>".$row['pareigos']."</td>";
                echo "<td><a href=\"egzistojancio_darbuotojo_informacija.php?id=".$row['id']."\">Perziureti pilna info</td>";
                echo "</tr>";
            }
        }

        function kurti_nauja_darbuotoja(){
            
            $sql = "SELECT * FROM darbuotoju_sarasas WHERE darbuotoju_sarasas.vardas = \"".$_POST['vardas']."\" AND darbuotoju_sarasas.pavarde = \"".$_POST['pavarde']."\""; # Kolkas tikrinu tiktai pagal varda ir pavarde
            $result = mysqli_query($this->con, $sql);
            $row = mysqli_fetch_assoc($result);
            echo $row;
            $arDupl = $this->tikrinti_ar_dublikatas($row);
            if($arDupl == true){
                header("Refresh: 1; http://localhost/Admin/index.php");
                echo "<script>alert('Toks darbuotojas jau egzistuoja. Uzkraunamas darbuotoju sarasas.');</script>";
                exit;
            }
            
            $lytis = 0;
            if($_POST['lytis'] == "M"){
                $lytis = 1;
            }
            
            $kurjerioID = "NULL";
            if($_POST['pareigos'] == "kurjeris"){
                $sql_insert_kurjeris = "INSERT INTO `kurjeris` (`kodas`, `vardas`, `pavarde`, `lytis`, `telefonas`, `adresas`) VALUES (NULL, '".$_POST['vardas']."', '".$_POST['pavarde']."', '".$lytis."', '".$_POST['telefonas']."', '".$_POST['adresas']."')";
                mysqli_query($this->con, $sql_insert_kurjeris);
                # randam naujo kurjerio ID
                $result = mysqli_query($this->con, "SELECT kodas FROM kurjeris WHERE kurjeris.vardas = \"".$_POST['vardas']."\" AND kurjeris.pavarde = \"".$_POST['pavarde']."\"");
                $row = mysqli_fetch_assoc($result);
                $kurjerioID = $row['kodas'];
            }
            
            $sql_insert = "INSERT INTO `darbuotoju_sarasas` (`id`, `vardas`, `pavarde`, `lytis`, `pareigos`, `dirba_nuo`, `alga`, `gimimo_metai`, `asmens_kodas`, `adresas`, `telefonas`, `fk_kurjeris`)".
            " VALUES (NULL, '".$_POST['vardas']."', '".$_POST['pavarde']."', '".$_POST['lytis']."', '".$_POST['pareigos']."', '".$_POST['dirba_nuo']."','".$_POST['alga']."', '".$_POST['gimimo_metai']."', '".$_POST['asmens_kodas']."', '".$_POST['adresas']."', '".$_POST['telefonas']."', ".$kurjerioID.")";
            mysqli_query($this->con, $sql_insert);
            echo $sql_insert;
            header("location:http://localhost/Admin/index.php");
            #header("Refresh: 100; http://localhost/Admin/index.php");
        }

        function tikrinti_ar_dublikatas($mrow){
            if(!empty($mrow)){
                return true;
            }
            return false;
        }

        function perziureti_darbuotoja($id){    
            $sql = "SELECT * FROM darbuotoju_sarasas WHERE darbuotoju_sarasas.id = ".$id;
            $result = mysqli_query($this->con, $sql);
            $row = mysqli_fetch_assoc($result);
            if(empty($row)){
                header("Refresh: 1; http://localhost/Admin/index.php");
                echo "<script>alert('Toks darbuotojas neegzistuoja. Uzkraunamas ');</script>";
                exit;
            }
    
            echo "<h3>Rodoma darbuotojo \"".$row['pavarde']." ".$row['vardas']."\" informacija.</h3>";
            echo "<div class=\"form\" style=\"width: 600px\">";
            echo "<h4>Darbuotojo vardas pavarde: ".$row['vardas']." ".$row['pavarde']."<br>";
            echo "Darbuotojo ID: ".$row['id']."<br>";
            echo "Pareigos: ".$row['pareigos']."<br>";
            echo "Adresas: ".$row['adresas']."<br>";
            echo "Tel. numeris: ".$row['telefonas']."<br>";
            echo "Alga:".$row['alga']."<br>";
            echo "Gimimo metai:".$row['gimimo_metai']."<br>";
            echo "Dirba nuo:".$row['dirba_nuo']."<br>";
            echo "</h4></div>";
            echo "<a class=\"marginright\" href=\"naikinti_darbuotoja.php?id=".$row['id']."\">Naikinti darbuotoja</a>";
            echo "<a class=\"marginright\" href=\"atnaujinti_informacija.php?id=".$row['id']."\">Atnaujinti darbuotoja</a><br>";
        }

        function atnaujinti_informacija($id){
            $sql = "SELECT * FROM darbuotoju_sarasas WHERE darbuotoju_sarasas.id = ".$id;
            $result = mysqli_query($this->con, $sql);
            $row = mysqli_fetch_assoc($result);
            if(empty($row)){
                header("Refresh: 1; http://localhost/Admin/index.php");
                echo "<script>alert('Darbuotojas su ID ".$id." nerastas ');</script>";
                exit;
            }
          
          
            echo "<h3>Rodoma darbuotojo \"".$row['pavarde']." ".$row['vardas']."\" informacija.</h3><hr>";
          
            echo "<form action=\"atnaujinti_darbuotoja_action.php?id=".$id."\" method=\"post\">";
            echo "Vardas:<br><input class=\"form-control\" type=\"text\" name=\"vardas\" value=\"".$row['vardas']."\"><br>";
            echo "Pavarde:<br><input class=\"form-control\" type=\"text\" name=\"pavarde\" value=\"".$row['pavarde']."\"><br>";
            #echo "Lytis:<br>";
            #echo "<select name=\"lytis\">";
            #echo "<option value=\"V\">Vyras</option>";
            #echo "<option value=\"M\">Moteris</option>";
            #echo "</select>";
            #echo "<br><br>";
            echo "Pareigos:<br><input class=\"form-control\" type=\"text\" name=\"pareigos\" value=\"".$row['pareigos']."\"><br>";
            #echo "Dirba nuo:<br><input class=\"form-control\" type=\"date\" name=\"dirba_nuo\" value=\"".$row['dirba_nuo']."\"><br>";
            echo "Adresas:<br><input class=\"form-control\" type=\"text\" name=\"adresas\" value=\"".$row['adresas']."\"><br>";
            echo "Alga:<br><input class=\"form-control\" type=\"number\" name=\"alga\" value=".$row['alga']."><br>";
            #echo "Gimimo metai:<br><input class=\"form-control\" type=\"date\" name=\"gimimo_metai\" value=\"".$row['gimimo_metai']."\"><br>";
            echo "Telefono numeris:<br><input class=\"form-control\" type=\"number\" name=\"telefonas\" value=\"".$row['telefonas']."\"><br>";
            #echo "Asmens kodas:<br><input class=\"form-control\" type=\"number\" name=\"asmens_kodas\" value=".$row['asmens_kodas']."><br>";
            echo "<input type=\"submit\" value=\"Atnaujinti\"></form>";
        }

        function panaikinti_darbuotoja($id){
            $sql = "DELETE FROM `darbuotoju_sarasas` WHERE `darbuotoju_sarasas`.`id` = ".$id;
            $result = mysqli_query($this->con, $sql);
            echo $result;
            #header("Refresh: 100; http://localhost/Admin/index.php");
            header("location:http://localhost/Admin/index.php");
        }
    }

?>