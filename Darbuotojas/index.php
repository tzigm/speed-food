<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Speed food</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.table td {
   text-align: center;
}
#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
</style>
</head>
<body background="css/bg1.jpg">
<div id="alls">
<div id="alls2">
  <div id="customheader">
        <div class="container">
          <h2>Speed food</h2>
          <p>Maisto užsakymo į namus sistema</p>
        </div>
  </div>
  
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Speed Food</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Gauti užsakymai</a></li>
        <li><a href="worksheet.php">Darbo grafikas</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
      </ul>
    </div>
  </nav>

  <div class="container">

  <h3>Nepriimti užsakymai:</h3>
  <?php
  $vardas=$_SESSION['username'];
  $date = date('Y-m-d');

  $query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.busena=0";
  $result = mysqli_query($con,$query) or die(mysql_error());
   if (!$result || (mysqli_num_rows($result) < 1))
       {echo "Nėra užsakymų";}
       else{
              echo "<table class=\"table table-bordered\">";
             echo "<tr bgcolor=\"#CCC\">
             <td class=\"col-md-1\"></td>
              <td class=\"col-md-3\">Adresas</td>
              <td class=\"col-md-3\">Suma</td>
              </tr>";
         while($row = mysqli_fetch_assoc($result))
   {
     unset($address,$price,$orderId);
              $orderId = $row['id'];
              $address = $row['adresas'];
              $price = $row['suma'];
          echo "<tr>
          <td><form method=\"POST\"action=\"acceptorder.php\">
          <input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
          <input type=\"hidden\" name=\"address\" value=\"$address\">
          <input type=\"submit\" value=\"Priimti užsakymą\" />
          </form></td>
          <td>".$address."</td>
          <td>".$price."</td>
          </tr>";
        }
  }
  ?>
  </div>
  </div>
  </div>
  <footer id="footer">
  </footer>
</body>
</html>
