<?php
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Speed food</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
		#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
		</style>
	</head>
	<body background="css/bg1.jpg">
	<div id="alls">
	<div id="alls2">
		<div id="customheader">
			<div class="container">
				<h2>Speed food</h2>
				<p>Maisto užsakymo į namus sistema</p>
			</div>
		</div>
		
		<nav id="menu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">Speed Food</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="../Klientas/index.php">Kurjerių vertinimas</a></li>
					<li><a href="../Klientas/productevaluation.php">Prekių vertinimas</a></li>
					<li class="active"><a href="payment-list.php">Apmokėjimų sąrašas</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../Klientas/logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
				</ul>
			</div>
		</nav>
		<?php
		$id=$_POST['orderid'];
		$query = "SELECT uzsakymai.id AS id, uzsakymai.adresas AS adresas, uzsakymai.suma AS suma, uzsakymai.valiuta AS valiuta, uzsakymai.priimtas_data AS priimtas_data, uzsakymai.priimtas_laikas AS priimtas_laikas, uzsakymai.pristatytas_data AS pristatytas_data, uzsakymai.pristatytas_laikas AS pristatytas_laikas, kurjeriai.vardas AS vardas, kurjeriai.pavarde AS pavarde, kurjeriai.telefonas AS telefonas FROM uzsakymai,kurjeriai WHERE `uzsakymai`.id='$id' AND uzsakymai.fk_kurjerio_id=kurjeriai.id";
		
				$result = mysqli_query($con,$query) or die(mysql_error());
				
				if (!$result || (mysqli_num_rows($result) < 1))
				{echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";}
				else{
					echo "<table class=\"table table-bordered\">";
					echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
							<td>Užsakymo data</td>
							<td>Užsakymo laikas</td>
							<td>Pristatymo data</td>
							<td>Pristatymo laikas</td>
							<td>Kurjerio vardas</td>
							<td>Kurjerio pavardė</td>
							<td>Kurjerio telefono numeris</td>
							
						</tr>";
					while($row = mysqli_fetch_assoc($result))
         {
           unset($addres,$price,$date,$time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
					$valiuta = $row['valiuta'];
                    $date=$row['priimtas_data'];
                    $time=$row['priimtas_laikas'];
					$date1=$row['pristatytas_data'];
					$time1=$row['pristatytas_laikas'];
                    $time = date("H:i", strtotime("$time"));
					$time1 = date("H:i", strtotime("$time1"));
					$kurjerioVardas = $row['vardas'];
					$kurjerioPavarde = $row['pavarde'];
					$telefonas = $row['telefonas'];
                echo "<tr>
                <td>".$orderId."</td>
                <td>".$addres."</td>
                <td>".$price."</td>
				<td>".$valiuta."</td>
				<td>".$date."</td>
				<td>".$time."</td>
				<td>".$date1."</td>
				<td>".$time1."</td>
				<td>".$kurjerioVardas."</td>
				<td>".$kurjerioPavarde."</td>
				<td>".$telefonas."</td>
                </tr>";
          }
		  echo "</table>";
		  //ISRENKAM VISAS PREKES IS TO UZSAKYMO
		  $query = "SELECT prekes.id AS id, prekes.pavadinimas AS pavadinimas, prekes.kiekis AS kiekis, prekes.kaina AS kaina, prekes.galiojimas AS galiojimas, prekes.pagaminimo_data AS pagaminimas, prekes.kategorija AS kategorija FROM prekes,uzsakymai WHERE `uzsakymai`.id='$id' AND uzsakymai.id=prekes.fk_krepselio_id";
		
				$result1 = mysqli_query($con,$query) or die(mysql_error());
				
				if (!$result1 || (mysqli_num_rows($result1) < 1))
				{}
				else{
					echo "<table class=\"table table-bordered\">";
					echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Pavadinimas</td>
							<td>Kiekis</td>
							<td>Kaina</td>
							<td>Galiojimo pasibaigimo data</td>
							<td>Pagaminimo data</td>
							<td>Kategorija</td>
							
						</tr>";
					while($row = mysqli_fetch_assoc($result1))
         {
           unset($addres,$price,$date,$time);
                    $prekesId = $row['id'];
					$pavadinimas = $row['pavadinimas'];
                    $kiekis = $row['kiekis'];
					$kaina = $row['kaina'];
					$galiojimas = $row['galiojimas'];
					$pagaminimas = $row['pagaminimas'];
					$kategorija = $row['kategorija'];
                echo "<tr>
					<td>".$prekesId."</td>
				    <td>".$pavadinimas."</td>
				    <td>".$kiekis."</td>
				    <td>".$kaina."</td>
				    <td>".$galiojimas."</td>
				    <td>".$pagaminimas."</td>
				    <td>".$kategorija."</td>
                </tr>";
          }
				}
				echo "</table>";
				}
		?>
		</div>
		</div>
		<footer id="footer">
		</footer>
	</body>
</html>