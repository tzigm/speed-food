<?php
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Speed food</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
		#filtras {
			width: 100%;
			height: 50px;
			float:right;
	
			background: rgba(255, 255, 255, 0.5);			
		}
		#menu {
			margin:0px;
			background: rgba(255, 255, 255, 0.5);
			border-radius:0px;
			border-bottom:1px solid black;
		}
		#dropas {
			margin-top:1px;
			height:48px;
			float:left;
			border:0px;
			background: rgba(255, 255, 255, 0.3);
			font-size:150%;
			color:#777777;
		
		}
		#textas {
			margin-top: 10px;
			font-size: 150%;
			margin-left:5px;
			color:#777777;
			float: left;
		}
		#div1{
			padding-top:7px;
			margin-top:0px;
			margin-left:5px;
			height:48px;
			float:left;
			border:0px;
			background: rgba(255, 255, 255, 0.3);
			font-size:150%;
			color:#777777;
		}
		#div2{
			padding-top:7px;
			margin-top:0px;
			margin-left:5px;
			height:48px;
			float:left;
			border:0px;
			background: rgba(255, 255, 255, 0.3);
			font-size:150%;
			color:#777777;
		}
		#div3{
			padding-top:0px;
			margin-top:0px;
			margin-left:5px;
			height:48px;
			float:left;
			border:0px;
			background: rgba(255, 255, 255, 0.3);
			font-size:100%;
			color:#777777;
		}
		#div4{
			position:relative;
			top:-6px;
			padding-top:0px;
			margin-top:0px;
			margin-left:5px;
			height:50px;
			float:left;
			border:0px;
			background: rgba(255, 255, 255, 0.3);
			font-size:100%;
			color:#777777;
		}
		
		#filt{
			padding-top:0px;
			margin-top:0px;
			margin-left:5px;
			height:50px;
			float:left;
			
			background: rgba(255, 255, 255, 0.3);
			font-size:150%;
			color:#777777;
			border:1px solid black;
			border-top:0px;
		}
		#te {
			float:right;
			font-size:200%;
		}
		#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
		
		.table td {
   text-align: center;
   height:10px;
   
		</style>
		
	</head>
	<body background="css/bg1.jpg">
	<div id="alls">
	<div id="alls2">
		<div id="customheader">
			<div class="container">
				<h2>Speed food</h2>
				<p>Maisto užsakymo į namus sistema</p>
			</div>
		</div>
		
		<nav id="menu" class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">Speed Food</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="../Klientas/index.php">Kurjerių vertinimas</a></li>
					<li><a href="../Klientas/productevaluation.php">Prekių vertinimas</a></li>
					<li class="active"><a href="payment-list.php">Apmokėjimų sąrašas</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../Klientas/logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
				</ul>
			</div>
		</nav>
		<div id="filtras">
			<p id="textas">
			Rūšiuoti pagal:
			</p>
			<select id="dropas" onchange="showfield(this.options[this.selectedIndex].value)">
				<option class="optionai" value="0">----------------------</option>
				<option class="optionai" value="date">datą.</option>
				<option class="optionai" value="opt">mokėjimo būdą.</option>
				<option class="optionai" value="val">valiutą.</option>
			</select>
			<form>
			<div id="div1" name="nuo" value=''></div>
			<div id="div2" name="iki" value=''></div>
			<div id="div3" name="budas" value=''></div>
			<div id="div4" name="val" value=''></div>
			<button type="submit" id="filt">Filtruoti!</button>
			</form>
		</div>
		<?php
		
		 $vardas=$_SESSION['username'];
	
		require('db.php');
		
			if (isset($_GET['nuo']) && isset($_GET['iki']))
			{
				$nuo=$_GET['nuo'];
				$iki=$_GET['iki'];
				
				//traukiam is DB pagal data
				$query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas' AND uzsakymai.priimtas_data>'$nuo' AND uzsakymai.priimtas_data<'$iki'";
				$result = mysqli_query($con,$query) or die(mysql_error());
				
				if (!$result || (mysqli_num_rows($result) < 1))
				{echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";}
				else{
					echo "<table class=\"table table-bordered\">";
					echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
         while($row = mysqli_fetch_assoc($result))
         {
           unset($addres,$price,$date,$time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
                    $date=$row['priimtas_data'];
					$valiuta=$row['valiuta'];
                    $time=$row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                echo "<tr>
                <td>".$row['id']."</td>
                <td>".$row['adresas']."</td>
                <td>".$price."</td>
				<td>".$valiuta."</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
          }
  }
				
			}else if(isset($_GET['budas']))
			{
				$budas=$_GET['budas'];
				
				//traukiam is DB pagal data
				$query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas' AND uzsakymai.budas='$budas'";
				$result = mysqli_query($con,$query) or die(mysql_error());
				
				if (!$result || (mysqli_num_rows($result) < 1))
				{echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";}
				else{
					echo "<table class=\"table table-bordered\">";
					echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
         while($row = mysqli_fetch_assoc($result))
         {
           unset($addres,$price,$date,$time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
					$valiuta = $row['valiuta'];
                    $date=$row['priimtas_data'];
                    $time=$row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                echo "<tr>
                <td>".$row['id']."</td>
                <td>".$row['adresas']."</td>
                <td>".$price."</td>
				<td>".$valiuta."</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
          }
  }
				
				
			}else if(isset($_GET['val']))
			{
				//traukiam is DB pagal valiuta
				$val=$_GET['val'];
				
				//traukiam is DB pagal data
				$query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas' AND uzsakymai.valiuta='$val'";
				$result = mysqli_query($con,$query) or die(mysql_error());
				
				if (!$result || (mysqli_num_rows($result) < 1))
				{echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";}
				else{
					echo "<table class=\"table table-bordered\">";
					echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
         while($row = mysqli_fetch_assoc($result))
         {
           unset($addres,$price,$date,$time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
					$valiuta = $row['valiuta'];
                    $date=$row['priimtas_data'];
                    $time=$row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                echo "<tr>
                <td>".$row['id']."</td>
                <td>".$row['adresas']."</td>
                <td>".$price."</td>
				<td>".$valiuta."</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
          }
  }
			}else {
				//Traukiam is DB visus
				$query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas'";
				$result = mysqli_query($con,$query) or die(mysql_error());
				
				if (!$result || (mysqli_num_rows($result) < 1))
				{echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";}
				else{
					echo "<table style='margin-bottom:0px;'class=\"table table-bordered\">";
					echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
         while($row = mysqli_fetch_assoc($result))
         {
           unset($addres,$price,$date,$time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
					 $valiuta = $row['valiuta'];
                    $date=$row['priimtas_data'];
                    $time=$row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                echo "<tr>
                <td>".$row['id']."</td>
                <td>".$row['adresas']."</td>
                <td>".$price."</td>
				<td>".$valiuta."</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
          }
		  
  }
			}
			
			echo "</table>";
		?>
		</div>
		</div>
		<script type="text/javascript">
		function showfield(name){
			if(name=='date'){document.getElementById('div1').innerHTML='Nuo: <input type="date" name="nuo" />';document.getElementById('div2').innerHTML='Iki: <input type="date" name="iki" />';}
			else if(name=='opt'){document.getElementById('div3').innerHTML='<select id="dropas" name="budas"><option value="0">Grynais</option><option value="1">Pavedimu</option> </select>'; document.getElementById('div2').innerHTML='';document.getElementById('div1').innerHTML='';}
			else if(name=='val'){document.getElementById('div4').innerHTML='<input type="text" name="val">';document.getElementById('div3').innerHTML=''; document.getElementById('div2').innerHTML='';document.getElementById('div1').innerHTML='';}
			else {document.getElementById('div4').innerHTML='';document.getElementById('div3').innerHTML=''; document.getElementById('div2').innerHTML='';document.getElementById('div1').innerHTML='';}
		}
		
		
		</script>
		<footer id="footer">
		</footer>
	</body>
	
</html>
