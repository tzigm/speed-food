<?php
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<html>
<head>
    <meta charset="utf-8">
    <title>Speed food</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        #filtras {
            width: 100%;
            height: 50px;
            float:right;

            background: rgba(255, 255, 255, 1);
        }
        #menu {
            margin:0px;
            background: rgba(255, 255, 255, 1);
            border-radius:0px;
            border-bottom:1px solid black;
        }
        #dropas {
            margin-top:1px;
            height:48px;
            float:left;
            border:0px;
            background: rgba(255, 255, 255, 1);
            font-size:150%;
            color:#777777;

        }
        #textas {
            margin-top: 10px;
            font-size: 150%;
            margin-left:5px;
            color:#777777;
            float: left;
        }
        #filt{
            padding-top:0px;
            margin-top:0px;
            margin-left:5px;
            height:50px;
            float:left;

            background: rgba(255, 255, 255, 1);
            font-size:150%;
            color:#777777;
            border:1px solid black;
            border-top:0px;
        }
        #te {
            float:right;
            font-size:200%;
        }
        #alls{
            min-height:100%;
            margin 0px;
        }
        #alls2{
            overflow: auto;
            padding-bottom:100px;
        }

        .table td {
            text-align: center;
            height:10px;

    </style>

</head>
<body background="css/bg1.jpg">
<div id="alls">
    <div id="alls2">
        <div id="customheader">
            <div class="container">
                <h2>Speed food</h2>
                <p>Maisto užsakymo į namus sistema</p>
            </div>
        </div>

        <nav  id="menu" class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">Speed Food</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Pradžia</a></li>
                    <li ><a href="ismoketi.php">Išmokėti atlygį</a></li>
                    <li ><a href="worksheet.php">Darbo grafikas</a></li>
                    <li ><a href="pinigine.php">Piniginė</a></li>
                    <li class="active"><a href="ataskaitos.php">Ataskaitos</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
                </ul>
            </div>
        </nav>
        <div>
            <?php
            echo "<table class=\"table table-bordered\">";
            echo "<tr bgcolor=\"#CCC\">
							<td>Pavadinimas</td>
							<td></td>
                            
						</tr>";
            echo"<tr>";
             echo"<td>Pagal atsiskaitymo būdą:</td>
				<td><form method=\"POST\"action=\"pagalbuda.php\">
			
				
				<input type=\"hidden\" name=\"ne1\" value=\"0\">
				<input type=\"submit\" value=\"Generuoti\" /> </form></td>";

            echo"</tr>";
            echo"<tr>";
            echo"<td>Atliktų atsiskaitymų pagal datą:</td>
				<td><form method=\"POST\"action=\"pagaldata.php\">
				<input type=\"date\" name=\"nuo\" value=\"\">
				<input type=\"date\" name=\"iki\" value=\"\".>
				<input type=\"hidden\" name=\"budas\" value=\"2\">
				<input type=\"submit\" value=\"Generuoti\" />";
            echo"</tr>";
            echo "</table>";
            ?>
        </div>

    </div>
</div>
<footer id="footer">
</footer>
</body>

</html>
