
<html>
<head>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body>
<div id="chartContainer" style="width: 100%; height: 100%"></div>
<?php
include('../Atsiskaitymu_valdymas.php');
$controleris= new Atsiskaitymu_valdymas();
 if(isset($_POST['nuo']))
{
    $nuo=$_POST['nuo'];
    $iki=$_POST['iki'];

}
?>
<script>
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title:{
                text: "Atsiskaitymų kiekis pasirinktu laikotarpiu."
            },
            axisX:{
                valueFormatString: "DD MMM"
            },
            axisY: {
                title: "Atsiskaitymų kiekis",
                includeZero: true,
                scaleBreaks: {
                    autoCalculate: true
                }
            },
            data: [{
                type: "line",
                xValueFormatString: "DD MMM",
                color: "#7e69f0",
                dataPoints: [
                    <?php
                    $controleris->grazinti_statistika_pagal_data($nuo,$iki);
                    ?>
                ]
            }]
        });
        chart.render();

    }
</script>
</body>
</html>