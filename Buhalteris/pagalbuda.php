<?php
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<html>
<head>
    <meta charset="utf-8">
    <title>Speed food</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        #filtras {
            width: 100%;
            height: 50px;
            float:right;

            background: rgba(255, 255, 255, 1);
        }
        #menu {
            margin:0px;
            background: rgba(255, 255, 255, 1);
            border-radius:0px;
            border-bottom:1px solid black;
        }
        #dropas {
            margin-top:1px;
            height:48px;
            float:left;
            border:0px;
            background: rgba(255, 255, 255, 1);
            font-size:150%;
            color:#777777;

        }
        #textas {
            margin-top: 10px;
            font-size: 150%;
            margin-left:5px;
            color:#777777;
            float: left;
        }
        #filt{
            padding-top:0px;
            margin-top:0px;
            margin-left:5px;
            height:50px;
            float:left;

            background: rgba(255, 255, 255, 1);
            font-size:150%;
            color:#777777;
            border:1px solid black;
            border-top:0px;
        }
        #te {
            float:right;
            font-size:200%;
        }
        #alls{
            min-height:100%;
            margin 0px;
        }
        #alls2{
            overflow: auto;
            padding-bottom:100px;
        }

        .table td {
            text-align: center;
            height:10px;

    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>


<?php
include('../Atsiskaitymu_valdymas.php');
$controleris= new Atsiskaitymu_valdymas();
$dataPoints = $controleris->grazinti_statistika_budu();
?>






<div id="chartContainer" style="width: 100%; height: 100%"></div>
<script type="text/javascript">
    $(function () {
        var chart = new CanvasJS.Chart("chartContainer", {
            theme: "dark2",
            title:{
                text: "Atsiskaitymų kiekis tam tikrais būdais per visą laikotarpį."
            },
            exportFileName: "New Year Resolutions",
            exportEnabled: true,
            animationEnabled: true,
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{name}: <strong>{y}%</strong>",
                    indexLabel: "{name} {y}%",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
        });
        chart.render();
    });
</script>





</html>
