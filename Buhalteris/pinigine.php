<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Speed food</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.table td {
   text-align: center;
}

#alls{
			min-height:100%;
			margin 0px;
		}
		#alls2{
			overflow: auto;
			padding-bottom:100px;
		}
</style>
</head>
<body background="css/bg1.jpg">
<div id="alls">
<div id="alls2">

  <div id="customheader">
        <div class="container">
          <h2>Speed food</h2>
          <p>Maisto užsakymo į namus sistema</p>
        </div>
  </div>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Speed Food</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="index.php">Pradžia</a></li>
        <li><a href="ismoketi.php">Išmokėti atlygį</a></li>
        <li ><a href="worksheet.php">Darbo grafikas</a></li>
        <li class="active"><a href="">Piniginė</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Atsijungti</a></li>
      </ul>
    </div>
  </nav>
<div class="container">

<h3>Sąskaitų duomenys:</h3>
<?php
$date = date('Y-m-d');
$vardas=$_SESSION['username'];
$query = "SELECT * FROM `pinigines`,`darbuotojai` WHERE `darbuotojai`.id=`pinigines`.fk_darbuotojo_id AND `darbuotojai`.username='$vardas'";
$result = mysqli_query($con,$query) or die(mysql_error());
 if (!$result || (mysqli_num_rows($result) < 1))
     {echo "Dar neturite sąskaitų informacijos";}
     else{
            echo "<table id=\"table2\" class=\"table table-bordered\">";
           echo "<tr bgcolor=\"#CCC\">
            <td>Sąskaitos numeris</td>
            <td>Likutis</td>
            </tr>";
       while($row = mysqli_fetch_assoc($result))
 {
   unset($sask,$left);
            $sask = $row['sask_nr'];
            $left = $row['likutis'];
        echo "<tr>
        <td>".$sask."</td>
        <td>".$left."</td>
        </tr>";
      }
	  echo "</table>";
}
?>
</div>
</div>
</div>
<footer id="footer">
  </footer>
</body>
</html>
