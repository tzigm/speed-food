<?php
/**
 * Created by PhpStorm.
 * User: makon
 * Date: 28/11/2017
 * Time: 11:59
 */

class Atsiskaitymu_valdymas
{

    public function generuoti_atsiskaitymu_sarasa()
    {

        $con = mysqli_connect("localhost", "root", "", "testine");
        require('db.php');
        include("auth.php");

        $vardas = $_SESSION['username'];

        require('db.php');

        if (isset($_GET['nuo']) && isset($_GET['iki'])) {
            $nuo = $_GET['nuo'];
            $iki = $_GET['iki'];

            //traukiam is DB pagal data
            $query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas' AND uzsakymai.priimtas_data>'$nuo' AND uzsakymai.priimtas_data<'$iki'";
            $result = mysqli_query($con, $query) or die(mysql_error());

            if (!$result || (mysqli_num_rows($result) < 1)) {
                echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";
            } else {
                echo "<table class=\"table table-bordered\">";
                echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
                while ($row = mysqli_fetch_assoc($result)) {
                    unset($addres, $price, $date, $time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
                    $date = $row['priimtas_data'];
                    $valiuta = $row['valiuta'];
                    $time = $row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                    echo "<tr>
                <td>" . $row['id'] . "</td>
                <td>" . $row['adresas'] . "</td>
                <td>" . $price . "</td>
				<td>" . $valiuta . "</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
                }
            }

        } else if (isset($_GET['budas'])) {
            $budas = $_GET['budas'];

            //traukiam is DB pagal data
            $query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas' AND uzsakymai.budas='$budas'";
            $result = mysqli_query($con, $query) or die(mysql_error());

            if (!$result || (mysqli_num_rows($result) < 1)) {
                echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";
            } else {
                echo "<table class=\"table table-bordered\">";
                echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
                while ($row = mysqli_fetch_assoc($result)) {
                    unset($addres, $price, $date, $time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
                    $valiuta = $row['valiuta'];
                    $date = $row['priimtas_data'];
                    $time = $row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                    echo "<tr>
                <td>" . $row['id'] . "</td>
                <td>" . $row['adresas'] . "</td>
                <td>" . $price . "</td>
				<td>" . $valiuta . "</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
                }
            }


        } else if (isset($_GET['val'])) {
            //traukiam is DB pagal valiuta
            $val = $_GET['val'];

            //traukiam is DB pagal data
            $query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas' AND uzsakymai.valiuta='$val'";
            $result = mysqli_query($con, $query) or die(mysql_error());

            if (!$result || (mysqli_num_rows($result) < 1)) {
                echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";
            } else {
                echo "<table class=\"table table-bordered\">";
                echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
                while ($row = mysqli_fetch_assoc($result)) {
                    unset($addres, $price, $date, $time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
                    $valiuta = $row['valiuta'];
                    $date = $row['priimtas_data'];
                    $time = $row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                    echo "<tr>
                <td>" . $row['id'] . "</td>
                <td>" . $row['adresas'] . "</td>
                <td>" . $price . "</td>
				<td>" . $valiuta . "</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
                }
            }
        } else {
            //Traukiam is DB visus
            $query = "SELECT * FROM `uzsakymai` WHERE `uzsakymai`.fk_kliento_vardas='$vardas'";
            $result = mysqli_query($con, $query) or die(mysql_error());

            if (!$result || (mysqli_num_rows($result) < 1)) {
                echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";
            } else {
                echo "<table style='margin-bottom:0px;'class=\"table table-bordered\">";
                echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
						
							<td></td>
						</tr>";
                while ($row = mysqli_fetch_assoc($result)) {
                    unset($addres, $price, $date, $time);
                    $orderId = $row['id'];
                    $addres = $row['adresas'];
                    $price = $row['suma'];
                    $valiuta = $row['valiuta'];
                    $date = $row['priimtas_data'];
                    $time = $row['priimtas_laikas'];
                    $time = date("H:i", strtotime("$time"));
                    echo "<tr>
                <td>" . $row['id'] . "</td>
                <td>" . $row['adresas'] . "</td>
                <td>" . $price . "</td>
				<td>" . $valiuta . "</td>
				<td><form method=\"POST\"action=\"payment.php\">
				<input type=\"hidden\" name=\"orderid\" value=\"$orderId\">
				<input type=\"submit\" value=\"Detaliau\" />
                </form></td>
                </tr>";
                }

            }
        }

        echo "</table>";

    }
    public function generuoti_detalaus_atsiskaitymo_info($id){

        $con = mysqli_connect("localhost", "root", "", "testine");

        $query = "SELECT uzsakymai.id AS id, uzsakymai.adresas AS adresas, uzsakymai.suma AS suma, uzsakymai.valiuta AS valiuta, uzsakymai.priimtas_data AS priimtas_data, uzsakymai.priimtas_laikas AS priimtas_laikas, uzsakymai.pristatytas_data AS pristatytas_data, uzsakymai.pristatytas_laikas AS pristatytas_laikas, kurjeriai.vardas AS vardas, kurjeriai.pavarde AS pavarde, kurjeriai.telefonas AS telefonas FROM uzsakymai,kurjeriai WHERE `uzsakymai`.id='$id' AND uzsakymai.fk_kurjerio_id=kurjeriai.id";

        $result = mysqli_query($con,$query) or die(mysql_error());

        if (!$result || (mysqli_num_rows($result) < 1))
        {echo "<div id='te'>Dar neįvykdėte nei vieno užsakymo pagal tokį filtrą.</div>";}
        else{
            echo "<table class=\"table table-bordered\">";
            echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Adresas</td>
							<td>Suma</td>
							<td>Valiuta</td>
							<td>Užsakymo data</td>
							<td>Užsakymo laikas</td>
							<td>Pristatymo data</td>
							<td>Pristatymo laikas</td>
							<td>Kurjerio vardas</td>
							<td>Kurjerio pavardė</td>
							<td>Kurjerio telefono numeris</td>
							
						</tr>";
            while($row = mysqli_fetch_assoc($result))
            {
                unset($addres,$price,$date,$time);
                $orderId = $row['id'];
                $addres = $row['adresas'];
                $price = $row['suma'];
                $valiuta = $row['valiuta'];
                $date=$row['priimtas_data'];
                $time=$row['priimtas_laikas'];
                $date1=$row['pristatytas_data'];
                $time1=$row['pristatytas_laikas'];
                $time = date("H:i", strtotime("$time"));
                $time1 = date("H:i", strtotime("$time1"));
                $kurjerioVardas = $row['vardas'];
                $kurjerioPavarde = $row['pavarde'];
                $telefonas = $row['telefonas'];
                echo "<tr>
                <td>".$orderId."</td>
                <td>".$addres."</td>
                <td>".$price."</td>
				<td>".$valiuta."</td>
				<td>".$date."</td>
				<td>".$time."</td>
				<td>".$date1."</td>
				<td>".$time1."</td>
				<td>".$kurjerioVardas."</td>
				<td>".$kurjerioPavarde."</td>
				<td>".$telefonas."</td>
                </tr>";
            }
            echo "</table>";
            //ISRENKAM VISAS PREKES IS TO UZSAKYMO
            $query = "SELECT prekes.id AS id, prekes.pavadinimas AS pavadinimas, prekes.kiekis AS kiekis, prekes.kaina AS kaina, prekes.galiojimas AS galiojimas, prekes.pagaminimo_data AS pagaminimas, prekes.kategorija AS kategorija FROM prekes,uzsakymai WHERE `uzsakymai`.id='$id' AND uzsakymai.id=prekes.fk_krepselio_id";

            $result1 = mysqli_query($con,$query) or die(mysql_error());

            if (!$result1 || (mysqli_num_rows($result1) < 1))
            {}
            else{
                echo "<table class=\"table table-bordered\">";
                echo "<tr bgcolor=\"#CCC\">
							<td>ID</td>
							<td>Pavadinimas</td>
							<td>Kiekis</td>
							<td>Kaina</td>
							<td>Galiojimo pasibaigimo data</td>
							<td>Pagaminimo data</td>
							<td>Kategorija</td>
							
						</tr>";
                while($row = mysqli_fetch_assoc($result1))
                {
                    unset($addres,$price,$date,$time);
                    $prekesId = $row['id'];
                    $pavadinimas = $row['pavadinimas'];
                    $kiekis = $row['kiekis'];
                    $kaina = $row['kaina'];
                    $galiojimas = $row['galiojimas'];
                    $pagaminimas = $row['pagaminimas'];
                    $kategorija = $row['kategorija'];
                    echo "<tr>
					<td>".$prekesId."</td>
				    <td>".$pavadinimas."</td>
				    <td>".$kiekis."</td>
				    <td>".$kaina."</td>
				    <td>".$galiojimas."</td>
				    <td>".$pagaminimas."</td>
				    <td>".$kategorija."</td>
                </tr>";
                }
            }
            echo "</table>";
        }


    }
    public function pasirinkti_pervedima ($pareigos,$id,$n,$ik)
    {

        // If form submitted, insert values into the database
        if($pareigos=='kurjeris'){
            $this->moketi_kurjeris($id,$n,$ik);


        }
        if($pareigos=='buhalteris'){
           $this->moketi_buhalteris($id,$n,$ik);


        }
    }
    public function moketi_kurjeris($idas,$nuo,$iki){
        require('db.php');
        $con = mysqli_connect("localhost", "root", "", "testine");
        $query = "SELECT * FROM `kurjeriai`,`pinigines` WHERE `kurjeriai`.id='$idas' AND `pinigines`.fk_savininko_id='$idas';";
        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $kodas=$row['kodas'];
        $likutis=$row['likutis'];
        $query = "SELECT COUNT(`darbo_grafikai`.fk_kurjerioid) AS dienos FROM `kurjeriai`,`darbo_grafikai` WHERE `kurjeriai`.id='$idas' AND `darbo_grafikai`.fk_kurjerioid='$idas' AND `darbo_grafikai`.data>='$nuo' AND `darbo_grafikai`.data<='$iki';";
        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $dienos=$row['dienos'];
            if($dienos==0){

            echo "
						<div id=\"customheader\">
						      <div class=\"container\">
						        <h2>Speed food</h2>
						        <p>Maisto užsakymo į namus sistema</p>
						      </div>
						</div>
						<div class='form'><h3>Darbuotojas nurodytu laikotarpiu nedirbo.</h3><br/> <a href='index.php'>Grįžti.</a></div>";
        }else {

                $query = "SELECT COUNT(`biliotenis`.fk_kurjeris) AS bilietai FROM `kurjeriai`,`biliotenis` WHERE `kurjeriai`.id='$idas' AND `biliotenis`.fk_kurjeris='$idas' AND `biliotenis`.data>='$nuo' AND `biliotenis`.data<='$iki';";
                $result = mysqli_query($con, $query) or die(mysql_error());
                $row = mysqli_fetch_assoc($result);
                $bilietai = $row['bilietai'];
                $alga = ($dienos - $bilietai) * 30 + $likutis;


                $query = "UPDATE `pinigines` SET `likutis`='$alga' WHERE `pinigines`.fk_savininko_id='$idas' AND `pinigines`.kodas='$kodas';";
                $result = mysqli_query($con, $query);
                if ($result) {

                    echo "
						<div id=\"customheader\">
						      <div class=\"container\">
						        <h2>Speed food</h2>
						        <p>Maisto užsakymo į namus sistema</p>
						      </div>
						</div>
						<div class='form'><h3>Pavedimas sėkmingas.</h3><br/> <a href='index.php'>Grįžti.</a></div>";


                }
                $query = "INSERT INTO `atlygiai` VALUES (NULL ,$nuo,$iki,($dienos - $bilietai) * 30,'kurjeris',$idas);";
                print_r($nuo);
                $result = mysqli_query($con, $query);
            }

    }
    public function moketi_buhalteris($idas,$nuo,$iki){
        require('db.php');
        $con = mysqli_connect("localhost", "root", "", "testine");
        $query = "SELECT * FROM `darbuotojai`,`pinigines` WHERE `darbuotojai`.id='$idas' AND `pinigines`.fk_darbuotojo_id='$idas';";
        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $kodas=$row['kodas'];
        $likutis=$row['likutis'];
        $query = "SELECT COUNT(`darbo_grafikai`.fk_buhalterioid) AS dienos FROM `darbuotojai`,`darbo_grafikai` WHERE `darbuotojai`.id='$idas' AND `darbo_grafikai`.fk_buhalterioid='$idas' AND `darbo_grafikai`.data>='$nuo' AND `darbo_grafikai`.data<='$iki';";
        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $dienos=$row['dienos'];
        if($dienos==0){

            echo "
						<div id=\"customheader\">
						      <div class=\"container\">
						        <h2>Speed food</h2>
						        <p>Maisto užsakymo į namus sistema</p>
						      </div>
						</div>
						<div class='form'><h3>Darbuotojas nurodytu laikotarpiu nedirbo.</h3><br/> <a href='index.php'>Grįžti.</a></div>";
        }else{
        $query = "SELECT COUNT(`biliotenis`.fk_darbuotojas) AS bilietai FROM `darbuotojai`,`biliotenis` WHERE `darbuotojai`.id='$idas' AND `biliotenis`.fk_darbuotojas='$idas' AND `biliotenis`.data>='$nuo' AND `biliotenis`.data<='$iki';";
        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $bilietai=$row['bilietai'];
        $alga=($dienos-$bilietai)*30+$likutis;



        $query = "UPDATE `pinigines` SET `likutis`='$alga' WHERE `pinigines`.fk_darbuotojo_id='$idas' AND `pinigines`.kodas='$kodas';";
        $result = mysqli_query($con,$query);
        if($result){
            echo "
						<div id=\"customheader\">
						      <div class=\"container\">
						        <h2>Speed food</h2>
						        <p>Maisto užsakymo į namus sistema</p>
						      </div>
						</div>
						<div class='form'><h3>Pavedimas sėkmingas.</h3><br/> <a href='index.php'>Grįžti.</a></div>";
        }}
    }
    public function grazinti_statistika_budu(){

        $con = mysqli_connect("localhost", "root", "", "testine");

        $query = "SELECT COUNT(`uzsakymai`.budas) AS budas FROM `uzsakymai` WHERE `uzsakymai`.budas='0';";

        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $budas=$row['budas'];
        $query = "SELECT COUNT(`uzsakymai`.budas) AS budas FROM `uzsakymai` WHERE `uzsakymai`.budas='1';";

        $result = mysqli_query($con,$query) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        $budas1=$row['budas'];
        $count=$budas+$budas1;


        return array(array("y" => floor($budas/$count*100), "name" => "Pavedimas"),array("y" => floor($budas1/$count*100)+1, "name" => "Grynieji"));
    }
    public function grazinti_statistika_pagal_data($nuo, $iki){

        $from= new DateTime($nuo);
        $to= new DateTime($iki);
        $con = mysqli_connect("localhost", "root", "", "testine");
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($from, $interval, $to);
        foreach ( $period as $dt ){
            $query = "SELECT COUNT(`uzsakymai`.budas) AS budas FROM `uzsakymai` WHERE `uzsakymai`.priimtas_data='$dt';";
            $result = mysqli_query($con,$query) or die(mysql_error());
            $row = mysqli_fetch_assoc($result);
            $budas=$row['budas'];
            $ar=array("x" => $dt, "y" => $budas);
            $ful[]=array_push($ar);
        }
        return $ful;
    }
}

?>